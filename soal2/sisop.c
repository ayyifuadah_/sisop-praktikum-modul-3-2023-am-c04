//sisop
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <time.h>

unsigned long long factorial(unsigned long long n){
    if (n >=1)
    	return n * factorial(n-1);
    else
    	return 1;
}

int main() {
		//clock to count time used
    clock_t start, end;
    double cpu_time_used;
	start = clock();
	
    int segmentId;
    int *sharedCarr;
    key_t key = 1928; 

    /* Allocate shared memory segment */
    const int shareSize = sizeof(int) * 4 * 5;  // size of Carr array
    segmentId = shmget(key, shareSize, IPC_CREAT | 0666);

    /* Attach the shared memory segment */
    sharedCarr = (int *)shmat(segmentId, NULL, 0);

    /* Read the contents of shared memory into a local array */
    int Carr[4][5];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            Carr[i][j] = sharedCarr[i * 5 + j];
        }
    }

    /* Print the contents of Carr */
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", Carr[i][j]);
        }
        printf("\n");
    }
    
    int rows = sizeof(Carr) / sizeof(Carr[0]);
    int cols = sizeof(Carr[0]) / sizeof(Carr[0][0]);
    unsigned long long fact[rows][cols];
    
    for (int i=0; i<rows; i++) {
    	for (int j=0; j<cols; j++) {
    	   fact[i][j] = factorial(Carr[i][j]);
    	   printf("%lld ", fact[i][j]);
    	}
    }
	
    /* Detach shared memory segment */
    shmdt(sharedCarr);
    /* Remove shared memory segment */
    shmctl(segmentId, IPC_RMID, NULL);
    
    	//clock to count time
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("\nnon Multi-threading took %f seconds \n", cpu_time_used);

    return 0;
}
