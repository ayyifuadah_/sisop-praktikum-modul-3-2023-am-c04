# sisop-praktikum-modul-3-2023-AM-C04
Terdiri dari:
- Alfadito Aulia Denova | 5025211157 
- Layyinatul Fuadah | 5025211207 
- Armadya Hermawan S | 5025211243 

---

## Penjelasan Soal dan Dokumentasi
### Soal 1

---
### Soal 2
Pada soal ini diminta untuk melakukan operasi perkalian pada sebuah matriks berukuran 4x2 dan 2x5. Hasil dari matriks tersebut kemudian harus di pass ke program lain dengan menggunakan prinsip Shared memory. Kemudian, melakukan operasi faktorial dengan menggunakan multi-threading, dan membandingkannya dengan cara non-multi threading.

untuk menyelesaikan soal ini, diminta untuk kembuat tiga buah file yaitu kalian.c , cinta.c dan sisop.c.

kalian.c akan mengerjakan bagian perkalian matriksnya, cinta.c akan menerima hasil perkalian tersebut dan melakukan factorial dengan multi threading. sementara sisop.c juga akan mengambil hasil perkalian matriks dan melakukan operasi faktorial, tetapi tanpa multi threading. 

untuk melakukan perbandingan diantara cinta.c dan sisop.c saya mencoba menggunakan waktu sebagai parameter pembandingnya. 

bagian yang digunakan untuk menghitung waktu adalah
```c
clock_t start, end;
    double cpu_time_used;
	start = clock();

```
diawal

dan 
```c
end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("\nMulti-threading took %f seconds \n", cpu_time_used);

```
diakhir

dan untuk membaca shared memory maka dibuat lah code berikut pada kalian.c

```c
 /* Allocate shared memory segment */
    key_t key = 1928;
    const int shareSize = sizeof(int) * 4 * 5;  // size of Carr array
    int segmentId = shmget(key, shareSize, IPC_CREAT | 0666);

    /* Attach the shared memory segment */
    int *sharedCarr = (int *)shmat(segmentId, NULL, 0);

    /* Copy the contents of Carr to shared memory */
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            sharedCarr[i * 5 + j] = Carr[i][j];
        }
    }

    /* Rest of your code... */
	sleep(5);
    /* Detach shared memory segment */
    shmdt(sharedCarr);
    /* Remove shared memory segment */
    shmctl(segmentId, IPC_RMID, NULL);


```

dan untuk membaca dari shared memorynya dibuat potongan code berikut
```c
 int segmentId;
    int *sharedCarr;
    key_t key = 1928;     

    /* Allocate shared memory segment */
    const int shareSize = sizeof(int) * 4 * 5;  // size of Carr array
    segmentId = shmget(key, shareSize, IPC_CREAT | 0666);

    /* Attach the shared memory segment */
    sharedCarr = (int *)shmat(segmentId, NULL, 0);

```
tidak lupa kita tetap menutup shared memory setelah diakses.
```c
    /* Detach shared memory segment */
    shmdt(sharedCarr);
    /* Remove shared memory segment */
    shmctl(segmentId, IPC_RMID, NULL);
    
```

Pembuatan threading pada cinta.c memanfaatkan 'pthread.h` dan menggunakan sebuah struct data baru disebut param dan fungsi thread_func yang akan melakukan fungsi factorial biasa secara rekursif

```c
struct param_t {
	int num;
	unsigned long long result;
};

unsigned long long factorial(unsigned long long n){
    if (n >=1)
    	return n * factorial(n-1);
    else
    	return 1;
}

void *thread_func(void *args){
    struct param_t *param = (struct param_t *)args;
    param->result = factorial(param->num);
    return (void *)NULL;
}

```
penggunaannya dalam program main adalah sebagai berikut:
```c
 int rows = sizeof(Carr) / sizeof(Carr[0]);
    int cols = sizeof(Carr[0]) / sizeof(Carr[0][0]);
    pthread_t threads[rows][cols];
    struct param_t params[rows][cols];
    unsigned long long fact[rows][cols];
    
    for (int i=0; i<rows; i++) {
    	for (int j =0; j < cols; j++) {
    	   params[i][j].num = Carr[i][j];
    	   pthread_create(&threads[i][j], NULL, thread_func, &params[i][j]);
    	}
    }
    
    for (int i=0; i<rows; i++){
    	for(int j=0; j<cols; j++){
    	   pthread_join(threads[i][j], NULL);
    	   fact[i][j] = params[i][j].result;
    	   printf("%lld ", fact[i][j]);
       }
    }

```
disini saya curiga bahwa bagian inilah yang memberikan kontribusi waktu paling banyak pada program tersebut. Pada percobaan yang saya lakukan sebanyak 15 kali run, dengan dataset yang berbeda tiap kalinya, didapatkan rata-rata waktu yang dibutuhkan program dengan multi threading adalah 0,0037462 detik.

Sementara, waktu rata-rata untuk waktu yang dibutuhkan program tanpa multi threading adalah 0,0001439 detik. Padahal fungsi rekursif untuk faktorial sama. 

---



### Soal 3

---

### Soal 4

---

## Endnote
Banyak kekurangan dan kesalahan dalam pengerjaan laporan ini, mohon dimaklumi.
